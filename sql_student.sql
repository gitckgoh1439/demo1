
--https://github.com/in28minutes/spring-boot-examples/tree/master/spring-boot-2-myBatis-with-h2


CREATE TABLE students (
	id BIGINT(20) NOT NULL AUTO_INCREMENT,
	address VARCHAR(255) NOT NULL,
	age VARCHAR(255) NOT NULL,
	created_at DATETIME NOT NULL,
	emergency_contact VARCHAR(255) NOT NULL,
	ic VARCHAR(255) NOT NULL,
	name VARCHAR(255) NOT NULL,
	phone VARCHAR(255) NOT NULL,
	sex VARCHAR(255) NOT NULL,
	student_class VARCHAR(255) NOT NULL,
	student_code VARCHAR(255) NOT NULL,
	updated_at DATETIME NOT NULL,
	year VARCHAR(255) NOT NULL,
	PRIMARY KEY (id)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
AUTO_INCREMENT=3;


insert into students
(address,
	age,
	created_at,
	emergency_contact,
	ic,
	name,
	phone,
	sex,
	student_class,
	student_code,
	updated_at,
	year)
values
("DESA JAYA", "16", "2018-02-28 13:14:25", "mum 222", "ic_19999", "name123", "phone-909900", "MALE", "class_blue", "student_code4455", "2018-02-28 13:14:25", "YEAR-6")



