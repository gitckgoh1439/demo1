package com.school.dao;
import java.util.List;

import com.school.entity.Student;



public interface IStudentDAO {
    List<Student> getAllStudents();

    Student getStudentById(Long studentId);
    void addStudent(Student student);
    void updateStudent(Student student);
    void deleteStudent(Long studentId);
    boolean studentExists(String studentcode);
    
    
}
 