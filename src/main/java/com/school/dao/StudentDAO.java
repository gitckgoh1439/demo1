package com.school.dao;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.school.entity.Student;
import com.school.entity.StudentRowMapper;


@Transactional
@Repository
public class StudentDAO implements IStudentDAO {
	@Autowired
    private JdbcTemplate jdbcTemplate;
	
	
	@Override
	public Student getStudentById(Long studentId) {
		String sql = "SELECT * FROM swaggerstudent01 WHERE id = ?";
		//String sql = "SELECT student_code, name, ic, age FROM students WHERE id = ?";
		RowMapper<Student> rowMapper = new BeanPropertyRowMapper<Student>(Student.class);
		Student student = jdbcTemplate.queryForObject(sql, rowMapper, studentId);
		return student;
	}
	
	@Override
	public List<Student> getAllStudents() {
		String sql = "SELECT * FROM swaggerstudent01";
		RowMapper<Student> rowMapper = new StudentRowMapper();
		return this.jdbcTemplate.query(sql, rowMapper);
	}	

	
	@Override
	public void addStudent(Student student) {
		//Add article
		String sql = "INSERT INTO swaggerstudent01 (student_code, name, ic, age, sex, phone, address, year, student_class, emergency_contact, created_at, updated_at) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
		jdbcTemplate.update(sql, student.getStudentCode(), student.getName(), student.getIC(), student.getAge(), student.getSex(), student.getPhone(), student.getAddress(), student.getYear(), 
				student.getStudentClass(), student.getEmergencyContact(), student.getCreatedAt(), student.getUpdatedAt() );
		
		//Fetch article id
		sql = "SELECT id FROM swaggerstudent01 WHERE student_code = ?";
		long Id = jdbcTemplate.queryForObject(sql, long.class, student.getStudentCode());
		
		//Set article id 
		student.setId(Id);
		
	}
	
	@Override
	public void updateStudent(Student student) {
		String sql = "UPDATE swaggerstudent01 SET student_code=?, name=?, ic=?, age=?, sex=?, phone=?, address=?, year=?, student_class=?, emergency_contact=?, created_at=?, updated_at=? WHERE id=?";
		jdbcTemplate.update(sql, student.getStudentCode(), student.getName(), student.getIC(), student.getAge(), student.getSex(), student.getPhone(), student.getAddress(), student.getYear(), 
				student.getStudentClass(), student.getEmergencyContact(), student.getCreatedAt(), student.getUpdatedAt()
				, student.getId());
		
		//Fetch article id
		sql = "SELECT id FROM swaggerstudent01 WHERE student_code = ?";
		long Id = jdbcTemplate.queryForObject(sql, long.class, student.getStudentCode());
		
		//Set article id 
		student.setId(Id);
		
	}
	
	@Override
	public void deleteStudent(Long Id) {
		String sql = "DELETE FROM swaggerstudent01 WHERE id=?";
		jdbcTemplate.update(sql, Id);
	}
	
	
	@Override
	public boolean studentExists(String studentcode) {
		String sql = "SELECT count(*) FROM swaggerstudent01 WHERE student_code = ?";
		int count = jdbcTemplate.queryForObject(sql, Integer.class, studentcode);
		if(count == 0) {
    		return false;
		} else {
			return true;
		}
	}
	
	
}
