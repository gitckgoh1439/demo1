package com.school.entity;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class StudentRowMapper implements RowMapper<Student> {

	@Override
	public Student mapRow(ResultSet row, int rowNum) throws SQLException {
		Student student = new Student();
		
		student.setId(row.getLong("id"));

		student.setStudentCode(row.getString("student_code"));
		student.setName(row.getString("name"));
		student.setIC(row.getString("ic"));
		student.setAge(row.getString("age"));
		student.setSex(row.getString("sex"));
		student.setPhone(row.getString("phone"));
		student.setAddress(row.getString("address"));
		student.setYear(row.getString("year"));
		student.setStudentClass(row.getString("student_class"));
		student.setEmergencyContact(row.getString("emergency_contact"));

		student.setCreatedAt(row.getDate("created_at"));
		student.setUpdatedAt(row.getDate("updated_at"));
		
		
		return student;
		
	}


}
