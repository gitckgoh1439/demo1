package com.school.service;

import java.util.List;

import com.school.entity.Student;

public interface IStudentService {
     List<Student> getAllStudent();
    
     Student getStudentById(Long Id);
     boolean addStudent(Student student);
     void updateStudent(Student sudent);
     void deleteStudent(Long Id);
}
