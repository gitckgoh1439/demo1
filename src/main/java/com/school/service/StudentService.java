package com.school.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.school.dao.IStudentDAO;
import com.school.entity.Student;


@Service
public class StudentService implements IStudentService {
	@Autowired
	private IStudentDAO studentDAO;
	@Override
	public Student getStudentById(Long Id) {
		Student obj = studentDAO.getStudentById(Id);
		return obj;
	}	
	@Override
	public List<Student> getAllStudent(){
		return studentDAO.getAllStudents();
	}
	
	@Override
	public synchronized boolean addStudent(Student student){
       if (studentDAO.studentExists(student.getStudentCode())) {
    	   return false;
       } else {
    	   studentDAO.addStudent(student);
    	   return true;
       }
	}
	@Override
	public void updateStudent(Student student) {
		studentDAO.updateStudent(student);
	}
	@Override
	public void deleteStudent(Long Id) {
		studentDAO.deleteStudent(Id);
		
		
	}
}
